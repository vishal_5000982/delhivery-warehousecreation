const fs = require('fs');
var parse = require('csv-parse');
var http = require('http');
var promise = require('promise');
var request = require('sync-request');
var async = require('async');
var REQUEST = require('request');
var csvWriter = require('csv-write-stream');


fs.readFile("./load/data.csv", function (err, fileData) {
	var row = null;
	var writer = csvWriter();
	writer.pipe(fs.createWriteStream('./out.csv'))
	console.log("Reading the csv ", err, fileData);
  	parse(fileData, {columns: true}, function(err, output) {
  		console.log("Parsing the data ", err, output.length);

  		var passCount = 0;
  		var failCount = 0;
  		var errorCount = 0;
	
		for(var j = 1; j < 10; j++) {
			row = output[j];
			var inputData = row;
			console.log("Input Data",inputData)
			var data ={	phone: ''+inputData.phone,
					city: ''+inputData.city,
					name: ''+inputData.name,
					pin: ''+inputData.pincode,
					address: ''+inputData.address,
					country: 'India',
					contact_person: ''+inputData.contactperson,
					email: inputData.email,
					registered_name:''+inputData.registeredname
				  };

			

			var options = {
	  			url: 'https://test.delhivery.com/api/backend/clientwarehouse/create/',
	  			method : 'POST',
	  			headers: {
	    			'Authorization': 'Token 3afaa56c93ecb4dbea51563cf740eddaf926f9fe',
					'Content-Type': 'application/json'
	  			},
				body: data,
				json: true
			};

			console.log("Request ", JSON.stringify(options));
		
				

	        REQUEST(options, function (err, response,body) {
	            
	                // return callback(err || new Error(JSON.stringify(res.body)));
	                // console.log('error )
	                console.log(' [ERROR] : ' + err, "[STATUSCODE:] " + response.statusCode , " BODY- " + JSON.stringify(body));
	                // retryMerchant = retryMerchant.concat(midBatch)
	                // console.log(retryMerchant)
	            


					if(response && response.statusCode == 200) {
						++passCount;
						inputData['saved'] = true
						var passData = "REQUEST: "+JSON.stringify(data)+" \nRESPONSE: "+JSON.stringify(response.body)+"\n"+"\n ----------------------------------------------------------------------"+passCount+"------------------------------------------------------------------------------------------------------------------------";
						fs.appendFile("./logs/NewCOdes-delhivery-105-success.txt", passData, function(err) {
			    				if(err) {
			        				console.log(err);
			    				}
						});		
					} else {
						++failCount;
						inputData['saved'] = false
						var failData = "REQUEST: "+JSON.stringify(data)+" \nRESPONSE: "+JSON.stringify(response.body)+"\n"+"\n -----------------------------------------------------------------------"+failCount+"------------------------------------------------------------------------------------------------------------------";
						fs.appendFile("./logs/NewCOdes-delhivery-105-failure.txt", failData, function(err) {
			    				if(err) {
			        				console.log(err);
			    				}
						});
					}
					// console.log(inputData)
					console.log(inputData)
					writer.write(inputData)

	        });
	        
				// request('POST', 'https://test.delhivery.com/api/backend/clientwarehouse/create/',options);

				// console.log("Response Body ", response);
				// //console.log("//////////////");
		  		
		}	
  	});
  	// writer.end()
});
