"use strict";

const fs = require('fs');
var parse = require('csv-parse');
var async = require('async');
var REQUEST = require('request');

var urlMerchantWarehouseDetails = "http://merchantadminservice.mkt.paytm/v1/seller/common/internal/merchants.json?multi_warehouse=1&merchant_id=";
var batchSize = 30,
	midBatches = [],
	warehouses ={},
	retryMerchant = [];
fs.readFile("./testMerchantIds.csv", function (err, fileData) {
  	parse(fileData, {columns: true}, function(err, output) {
  		if(err){

  		}
  		// console.log(output)
  		var merchantIds = output.map(function(x) {
   			return x['merchant_id'];
		});
  		// output = output.values()
		console.log('Total Read :' + merchantIds.length)
        for(var i=0; i< Math.ceil(merchantIds.length/batchSize);i++){
            midBatches.push((merchantIds.slice(i*batchSize,(i+1)*batchSize)));
            // console.log(midBatches);
        }

        console.log('Total Number of batches: ' + midBatches.length)

	    var requestOpts = {
	        "url"    : urlMerchantWarehouseDetails,
	        "json"   : true,
	        "method" : "GET"
	    };

		  function escape_csv(x) {
		    if(x)
		      return ('' + x.replace(/"/g, '').replace(/,/g, ' ').replace(/\n/g," ").replace(/\r/g," ") + '');
		    else
		      return ('');
		  }
		
		function jsonToCsv(data, cb) {

		var keys = Object.keys(data[0]);
		var csv = [keys.join(",")];
		data.forEach(function (row) {
		  var csvRow = [];
		  keys.forEach(function (key) {
		    if (typeof row[key] === 'string') {
		      csvRow.push("" + escape_csv(row[key]) + "");
		    } else {
		      csvRow.push(row[key]);
		    }
		  });
		  csv.push(csvRow.join(","));
		});
		csv = csv.join("\n");
		return csv;
		}

        function processBatch(midBatch,callback){
        	requestOpts.url = urlMerchantWarehouseDetails + midBatch.join(',')
        	// console.log(requestOpts)
	        REQUEST(requestOpts, function (err, res,body) {
	            if (err || res.statusCode !== 200 || !body) {
	                // return callback(err || new Error(JSON.stringify(res.body)));
	                console.log('error while fetching merchnat details for batch: ' + midBatch.join(','))
	                console.log(' [ERROR] : ' + err)
	                retryMerchant = retryMerchant.concat(midBatch)
	                // console.log(retryMerchant)
	            }else{

		            body.forEach(function(merchantDetails){
		            	var merchant = merchantDetails.merchant
		            	var merchantWarehouse = merchantDetails.warehouse
		            	// console.log(merchant.id,merchantDetails.warehouse.length)
		            	merchantWarehouse.forEach(function(warehouse){
		            		// console.log(warehouse)
		            		if(warehouses[warehouse.id] === undefined){
		            			// console.log('here')
		            			// console.log(warehouse.id)
		            			var w_name = warehouse.name;
		            			if (!w_name || w_name.length<3){
		            				w_name = merchant.display_name
		            			}
		            			var data = {
		            				"name" : warehouse.id,
		            				"registeredname": w_name,
		            				"address" : warehouse.address,
		            				"contactperson": warehouse.contact_person || merchant.name,
		            				"email"	: warehouse.email || warehouse.alt_email || merchant.email_id,
		            				"phone"	: warehouse.phone || warehouse.alt_phone || merchant.mobile_no,
		            				"pincode": warehouse.pincode,
		            				"city"	: warehouse.city,
		            				"state"	: warehouse.state,
		            				"country" : 'India',
		            				"merchant-id" : merchant.id
		            			}
		            			warehouses[warehouse.id] = data;
		            			// console.log(warehouses)
		            		}
		            	});
		            console.log("Total Warehouse Processed: " + Object.keys(warehouses).length)
		            });	

	            }
	            
	            

	            callback();

	        });

        }
        function finalise(err){
        	// if err
            var warehousesvals = Object.keys(warehouses).map(function(key) {
			    return warehouses[key];
			});
            // console.log(warehousesvals);
            // console.log(retryMerchant)
            var retryData = retryMerchant
	        
	        if(warehousesvals.length>0){
	        	var reportingData = jsonToCsv(warehousesvals)
				fs.writeFile("./csv/waredhousedetails.csv", reportingData, 'utf8', function (err) {
				  if (err) {
				    console.log('Some error occured - file either not saved or corrupted file saved.');
				  } else{
				    console.log('Reporting Data saved!');
				  }
				});
	
	        }
	        
	        if(retryData.length>0){
	        	// retryData = retryData
	        	var csvretryData = retryData.map(function(x) {
					   return {'merchant_id':x};
					});
	        	csvretryData = jsonToCsv(csvretryData)
	        	// console.log(csvretryData)
				fs.writeFile("./csv/retrymerchants.csv", csvretryData, 'utf8', function (err) {
				  if (err) {
				    console.log('Some error occured - file either not saved or corrupted file saved.');
				  } else{
				    console.log('Retry Data saved!');
				  }
				});	
	        }
			
        }
        async.eachSeries(midBatches, processBatch, finalise);
  	});
});

